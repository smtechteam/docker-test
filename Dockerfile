FROM amazoncorretto:11-alpine-jdk
WORKDIR /app
COPY . .
RUN echo "$(ls -lart)"
RUN ./mvnw dependency:go-offline
RUN ./mvnw install
ARG WAR_FILE=target/*.war
COPY ${WAR_FILE} app.war
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.war"]