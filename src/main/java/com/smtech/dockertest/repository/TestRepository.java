package com.smtech.dockertest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smtech.dockertest.entity.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, Long>{

}
