package com.smtech.dockertest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smtech.dockertest.entity.Test;
import com.smtech.dockertest.service.TestService;

@RestController
@RequestMapping(path = "/test")
public class TestController {
	
	@Autowired
	private TestService testService;

	@GetMapping
	private Test test() {
		return testService.test();
	}
}
